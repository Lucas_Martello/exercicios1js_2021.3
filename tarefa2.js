function criarArrayRand(n){

    let array = []

    for (let i = 0; i < n; i++){
        array.push(Math.round(Math.random() * 100))
    }

    return array
}

function printArray(array){
    for (let i = 0; i < array.length; i++){
        console.log(array[i])
    }
}

function bubbleSort(array){
    for (let i = 0; i < array.length - 1; i++){
        for (let j = 0; j < array.length - 1 - i; j++){
            if (array[j] > array[j+1]){
                let temp = array[j]
                array[j] = array[j+1]
                array[j+1] = temp
            }
        }
    }
    return array
}

function printMatriz(matriz){
    for (let i = 0; i < matriz.length; i++){
        console.log(matriz[i])
    }
}

function mallocMatriz(linhas, colunas){

    let matriz = []

    for (let i = 0; i < linhas; i++){
        matriz.push(new Array(colunas))
    }

    return matriz
}

function multMatriz(matriz1, matriz2){

    let linhas = matriz1.length
    let colunas = matriz2[0].length

    let matrizNova = mallocMatriz(linhas, colunas)

    for (let i = 0; i < linhas; i++){
        for (let j = 0; j < colunas; j++){
            let soma = 0
            for (let k = 0; k < colunas; k++){
                soma += matriz1[i][k] * matriz2[k][j]
            }
            matrizNova[i][j] = soma
        }
    }
    return matrizNova
}

function main(){

    // exercicio 1: ordenar array

    const tam = 5
    let array = criarArrayRand(tam)
    console.log("array antes:")
    printArray(array)

    let arrayOrdenado = bubbleSort(array)
    console.log("\n-----------------\n")

    console.log("array depois: ")
    printArray(arrayOrdenado)

    console.log("\n---------------------------------------------------------------\n")

    // exercicio 2: multiplicar matrizes

    let matriz1 = [[2, -1], [2, 0]]
    let matriz2 = [[2, 3], [-2, 1]]

    let matriz3 = [[4, 0], [-1, -1]]
    let matriz4 = [[-1, 3], [2, 7]]

    matriz5 = multMatriz(matriz1, matriz2)
    printMatriz(matriz5)

    console.log("\n-----------------\n")

    matriz6 = multMatriz(matriz3, matriz4)
    printMatriz(matriz6)
 
}

main()