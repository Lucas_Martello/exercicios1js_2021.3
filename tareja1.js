// exercicios dia 1

function farenheitParaCelsius(farenheit){
    celsius = 5 * ((farenheit - 32) / 9)
    return celsius
}

function statusAluno(array){
    contador = 0
    soma = 0

    for (n of array){
        contador++
        soma += n
    }

    media = soma / contador
    console.log("situacao do aluno: ")

    if (media >= 6){
        console.log("aprovado")
    }
    else{
        console.log("reprovado")
    }

}

// já tinha feito esse exercicio antes e já sabia dessa solução de duas linhas
function fizzBuzz(numero){
    for (let i = 1; i <= numero; i++){
        console.log(`${i}: ` + ((i % 3 === 0 ? "Fizz" : "") + (i % 5 === 0 ? "Buzz" : "")))
    }
}


function main(){
    // exercicio farenheit
    let temperaturaFarenheit = 90 // 32,22 graus, segundo o google
    let temperaturaCelsius = farenheitParaCelsius(temperaturaFarenheit)
    console.log("temperatura celsius: " + temperaturaCelsius.toFixed(2))

    console.log("\n------------------------------\n")

    // exercicio aluno

    let notasAluno = [4, 5, 2] // seria reprovado
    statusAluno(notasAluno)

    console.log("\n------------------------------\n")

    // exercicio aluno   
    
    let numero = 15
    fizzBuzz(numero)
}

main()